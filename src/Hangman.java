import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Hangman {
    private String password;
    private ArrayList<String> passwordArray;
    private ArrayList<String> originalPasswordArray;
    private int failCounter;
    private int successCounter;

    private final int MAX_COUNTER = 10;
    private String hidePassword[];

    public Hangman(String password) {
        this.password = password;
        this.passwordArray = getPasswordArray(password);
        this.originalPasswordArray = getPasswordArray(password);
        this.failCounter = 0;
        this.successCounter = 0;
        this.hidePassword = new String[password.length()];
        generateHidePassword(password);

    }

    private static ArrayList<String> getPasswordArray(String password) {
        return new ArrayList<>(Arrays.asList(password.toLowerCase().split("")));
    }


    public int getSuccessCounter() {
        return this.successCounter;
    }

    public int getFailCounter() {
        return failCounter;
    }

    public void generateHidePassword(String password) {
        for (int i = 0; i < password.length(); i++) {
            this.hidePassword[i] = " - ";
        }
    }

    public void printHidePassword() {
        for (String s : hidePassword) {
            System.out.print(s);
        }
        System.out.println();
    }

    public void unHidePassowrd(String letter) {
        List<Integer> matchingIndices = new ArrayList<>();
        for (int i = 0; i < originalPasswordArray.size(); i++) {
            String element = originalPasswordArray.get(i);
            if (letter.equals(element)) {
                matchingIndices.add(i);
            }
        }
        for (int index : matchingIndices) {
            hidePassword[index] = letter;
        }


    }


    public boolean isEndGame(String letter) {
        printHidePassword();
        if (passwordArray.contains(letter)) {
            System.out.println("Score");
            unHidePassowrd(letter);
            passwordArray.removeIf(s -> s.matches(letter));
            this.successCounter++;
            printHidePassword();
        } else {
            System.out.println("Fail");
            printHang(failCounter);
            this.failCounter++;
            return MAX_COUNTER == failCounter;

        }
        return passwordArray.isEmpty();
    }

    public boolean isWin(){
        return passwordArray.isEmpty();
    }
    public void printHang(int number) {
        switch (number) {
            case 0:
                System.out.println("""
                                             
                                            
                                            
                                            
                                             
                                            
                         /                                 
                        """);
                break;
            case 1:
                System.out.println("""
                                             
                                            
                                            
                                            
                                             
                                            
                         /|\\                       
                        """);
                break;
            case 2:
                System.out.println("""
                          |                  
                          |                 
                          |                 
                          |                 
                          |                  
                          |                 
                         /|\\                 
                        """);
                break;
            case 3:
                System.out.println("""
                          |------------      
                          |                 
                          |                 
                          |                 
                          |                  
                          |                 
                         /|\\                 
                        """);
                break;
            case 4:
                System.out.println("""
                          |------------      
                          |           |     
                          |                
                          |                
                          |                  
                          |                 
                         /|\\                 
                        """);
                break;

            case 5:
                System.out.println("""
                          |------------      
                          |           |     
                          |           O     
                          |                 
                          |                  
                          |                 
                         /|\\                 
                        """);
            case 6:
                System.out.println("""
                          |------------      
                          |           |     
                          |           O     
                          |           |     
                          |                 
                          |                 
                         /|\\                 
                        """);
                break;

            case 7:
                System.out.println("""
                          |------------      
                          |           |     
                          |           O     
                          |           |\\   
                          |                 
                          |                 
                         /|\\                 
                        """);
                break;
            case 8:
                System.out.println("""
                          |------------      
                          |           |     
                          |           O     
                          |          /|\\   
                          |                  
                          |                 
                         /|\\                 
                        """);
                break;
            case 9:
                System.out.println("""
                          |------------      
                          |           |     
                          |           O     
                          |          /|\\   
                          |            \\    
                          |                 
                         /|\\                 
                        """);
                break;
            case 10:
                System.out.println("""
                          |------------      
                          |           |     
                          |           O     
                          |          /|\\   
                          |          / \\   
                          |                 
                         /|\\                 
                        """);
                break;
            default:
                break;
        }
    }


}
