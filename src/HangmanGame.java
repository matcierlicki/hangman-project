import java.util.*;

public class HangmanGame {
    public static void main(String[] args) {
        Hangman hangman = prepareGame();
        playGame(hangman);
    }

    private static void playGame(Hangman hangman) {
        Scanner scanner = new Scanner(System.in);
        hangman.printHidePassword();
        while (true) {
            System.out.println("Type letter");
            if (hangman.isEndGame(scanner.next())) {
                break;
            }
        }
        System.out.println("------------------------");
        System.out.println("End Game");
        System.out.println("You: " + (hangman.isWin() ? "Won" : "Fail"));
        System.out.println("You typed: " + (hangman.getSuccessCounter() + hangman.getFailCounter()) + " times");
        System.out.println("You typed fails: " + hangman.getFailCounter());
        System.out.println("You typed success: " + hangman.getSuccessCounter());
    }

    private static Hangman prepareGame() {
        List<String> passwords = Arrays.asList("Kot", "Pies", "Drabina", "Java", "Obraz", "Abstrakcyjna");
        Random random = new Random();
        int number = random.nextInt(passwords.size());
        String password = passwords.get(number);
        Hangman hangman = new Hangman(password);
        return hangman;
    }
}
